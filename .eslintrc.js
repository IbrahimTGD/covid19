module.exports = {
	"root": true,
	"env": {
		"node": true,
		"commonjs": true,
		"browser": true,
		"es6": true
	},
	"extends": [
		"eslint:recommended",
		"plugin:vue/recommended"
	],
	"parserOptions": {
		"parser": "babel-eslint",
		"ecmaVersion": 2018,
		"sourceType": "module"
	},
	"plugins": [
		"vue",
		"vuetify"
	],
	"rules": {
		"indent": ["error", "tab"],
		"no-tabs": "off",
		"vue/html-self-closing": "off",
		"vue/html-indent": "off",
		"vue/max-attributes-per-line": ["error", {
			"singleline": 20,
			"multiline": {
				"max": 20,
				"allowFirstLine": true
			}
		}],
		"no-console": "off",
		"vue/singleline-html-element-content-newline": "off",
		"vuetify/no-deprecated-classes": "error",
		"vuetify/grid-unknown-attributes": "error",
		"vuetify/no-legacy-grid": "error",
	}
}