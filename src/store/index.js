import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		rtl: false,
		lang: "en",
		loading: false,
	},
	mutations: {
		changeLang(state, value) {
			state.lang = value;
		},
		changeRTL(state, value) {
			state.rtl = value;
		},
		setLoading(state, value) {
			state.loading = value;
		},
	},
	actions: {
		changeLang(state, value) {
			this.commit("changeLang", value);
		},
		changeRTL(state, value) {
			this.commit("changeRTL", value);
		},
		setLoading(state, value) {
			this.commit("setLoading", value);
		},
	},
})
