import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from "../store";

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home
	},
	{
		path: '/about',
		name: 'About',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
	}
]


const router = new VueRouter({
	mode: process.env.NODE_ENV == "production" ? "history" : "hash",
	base: process.env.BASE_URL,
	routes,
	scrollBehavior(to) {
		if (to.hash) {
			const element = document.querySelector(to.hash);
			if (element) {
				return window.scrollTo({ top: element.offsetTop, behavior: 'smooth' });
			}
		}
		return window.scrollTo({ top: 0, behavior: 'smooth' });
	}
})

router.beforeEach((to, from, next) => {
	store.dispatch("setLoading", true);
	next();
});

// eslint-disable-next-line no-unused-vars
router.afterEach((to, from) => {
	//
});

router.beforeResolve((to, from, next) => {
	setTimeout(() => {
		store.dispatch("setLoading", false);
	}, 200);
	next();
});

export default router
